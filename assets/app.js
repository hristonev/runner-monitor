/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

$( document ).ready(function() {
    const statistics = $('.js-post-statistics');
    if(statistics.length){
        getStats(statistics);
        window.setInterval(() => {
            getStats()
        }, 1000 * 60 * 5);
    }
})

function getStats(statistics){
    if(!statistics){
        return
    }
    $.ajax({
        type: 'GET',
        url: statistics.data('endpoint'),
        dataType: 'json',
        success: function (data) {
            for (const [user, players] of Object.entries(data)) {
                $('span[data-id="' + user + '"]').text(players.length)
                const container = $('#stats' + user + ' .modal-body ul');
                container.empty();
                for(let key in players){
                    container.append('<li class="list-group-item d-flex justify-content-between align-items-center">' + players[key]['player'] + '<span class="badge badge-primary bg-primary">' + players[key]['passed'] + '</span></li>')
                }
            }
        }
    })
}
