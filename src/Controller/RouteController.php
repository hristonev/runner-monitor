<?php

namespace App\Controller;

use App\Entity\Route as RouteEntity;
use App\Entity\RouteUser;
use App\Entity\User;
use App\Form\RouteType;
use App\Form\RouteUserType;
use App\Repository\RouteRepository;
use App\Repository\RouteUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[isGranted(User::ROLE_ADMIN)]
class RouteController extends AbstractController
{
    #[Route('/routes', name: 'app_route_list')]
    public function index(RouteRepository $routeRepository): Response
    {
        return $this->render('route/index.html.twig', [
            'controller_name' => 'RouteController',
            'routes' => $routeRepository->findAll(),
        ]);
    }

    #[Route('/routes/create', name: 'app_route_create')]
    public function create(Request $request, RouteRepository $routeRepository): Response
    {
        $form = $this->createForm(RouteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var RouteEntity $route */
            $route = $form->getData();
            $route->setActive(false);
            $routeRepository->save($route, true);
            return $this->redirectToRoute('app_route_list');
        }

        return $this->render('route/form.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/routes/point/create/{id}', name: 'app_route_user_create')]
    public function edit(Request $request, RouteEntity $route, RouteUserRepository $repository): Response
    {
        $form = $this->createForm(RouteUserType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var RouteUser $routeUser */
            $routeUser = $form->getData();
            $routeUser->setRoute($route);
            $repository->save($form->getData(), true);
            return $this->redirectToRoute('app_route_list');
        }

        return $this->render('route/form_user.html.twig', [
            'form' => $form,
            'route' => $route
        ]);
    }

    #[Route('/routes/delete/{id}', name: 'app_route_delete')]
    public function delete(RouteEntity $route, RouteRepository $routeRepository): Response
    {
        $routeRepository->remove($route, true);
        return $this->redirectToRoute('app_user_list');
    }
}
