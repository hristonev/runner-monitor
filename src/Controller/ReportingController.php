<?php

namespace App\Controller;

use App\Form\RouteReportType;
use App\Repository\CheckpointRepository;
use App\Repository\RouteRepository;
use App\Service\ReportingService;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportingController extends AbstractController
{
    #[Route('/reporting', name: 'app_reporting')]
    public function index(
        Request $request,
        CheckpointRepository $checkpointRepository,
        ReportingService $reportingService,
    ): Response
    {
        $form = $this->createForm(RouteReportType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ui = $form->getData();
            $checkpoints = $checkpointRepository->getByPeriod(
                $ui['route'],
                Carbon::instance($ui['start'])->startOfDay(),
                Carbon::instance($ui['end'])->endOfDay(),
            );
            if($ui['publish']){
                $reportingService->publishCheckpointData($ui['route'], $checkpoints);
            }
            return $this->redirectToRoute('app_home');
        }

        return $this->render('reporting/index.html.twig', [
            'controller_name' => 'ReportingController',
            'form' => $form->createView(),
        ]);
    }
}
