<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[isGranted(User::ROLE_ADMIN)]
class UserController extends AbstractController
{
    #[Route('/users', name: 'app_user_list')]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/users/create', name: 'app_user_create')]
    public function create(
        Request $request,
        UserRepository $userRepository,
        UserPasswordHasherInterface $passwordHasher,
    ): Response
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $formData = $request->get('user');
            if(array_key_exists('roles', $formData)){
                foreach ($formData['roles'] as $role){
                    $user->addRole($role);
                }
            }
            $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));
            $userRepository->save($user, true);
            return $this->redirectToRoute('app_user_list');
        }

        return $this->render('user/form.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/users/delete/{id}', name: 'app_user_delete')]
    public function delete(User $user, UserRepository $userRepository): Response
    {
        $userRepository->remove($user, true);
        return $this->redirectToRoute('app_user_list');
    }
}
