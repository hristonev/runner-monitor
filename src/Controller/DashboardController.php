<?php

namespace App\Controller;

use App\Entity\Checkpoint;
use App\Entity\User;
use App\Form\CheckpointType;
use App\Form\RouteStartType;
use App\Repository\CheckpointRepository;
use App\Repository\RouteRepository;
use App\Service\DashboardService;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use App\Entity\Route as RouteEntity;

class DashboardController extends AbstractController
{
    private Request $request;
    private Response $redirect;

    public function __construct(
        private readonly DashboardService $dashboardService,
        private readonly RouteRepository $routeRepository,
        private readonly CheckpointRepository $checkpointRepository,
    ){}

    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(Request $request): Response
    {
        $this->request = $request;
        return match ($this->isGranted('ROLE_ADMIN')){
            true => $this->renderAdmin(),
            default => $this->renderUser($request),
        };
    }

    #[isGranted(User::ROLE_ADMIN)]
    #[Route('/dashboard/statistics', name: 'app_dashboard_statistics')]
    public function statistics(): JsonResponse
    {
        $data = [];

        $activeRoute = $this->dashboardService->getActiveRoute();
        $checkpoints = $this->checkpointRepository->findBy([
            'route' => $activeRoute,
        ]);
        foreach ($checkpoints as $checkpoint){
            $key = $checkpoint->getUser()->getId();
            if(!array_key_exists($key, $data)) {
                $data[$key] = [];
            }
            $date = $checkpoint->getCreated();
            $date->setTimezone(new \DateTimeZone('Europe/Sofia'));
            $data[$key][] = [
                'player' => $checkpoint->getPlayer(),
                'passed' => $date->format('H:i:s'),
            ];
        }

        return $this->json($data);
    }

    #[Route('/dashboard/checkpoint/delete/{id}', name: 'app_delete_checkpoint')]
    public function deleteCheckpoint(Checkpoint $checkpoint): Response
    {
        if($checkpoint->getUser()->getUserIdentifier() == $this->getUser()->getUserIdentifier()){
            $this->checkpointRepository->remove($checkpoint, true);
        }
        return $this->redirectToRoute('app_dashboard');
    }

    private function renderUser(): Response
    {
        $form = $this->createForm(CheckpointType::class);
        $route = $this->dashboardService->getActiveRoute();

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Checkpoint $checkpoint */
            $checkpoint = $form->getData();
            if(
                (int)$checkpoint->getPlayer() > 0 &&
                $checkpoint->getPlayer() <= $route->getNumberOfPlayers()
            ){
                $exist = $this->checkpointRepository->findOneBy([
                    'route' => $route,
                    'user' => $this->getUser(),
                    'player' => $checkpoint->getPlayer(),
                ]);
                if (is_null($exist)) {
                    $checkpoint->setUser($this->getUser());
                    $checkpoint->setRoute($route);
                    $checkpoint->setCreated(Carbon::now());
                    $this->checkpointRepository->save($checkpoint, true);
                }
            }
            return $this->redirectToRoute('app_dashboard');
        }
        return $this->render('dashboard/user.html.twig', [
            'controller_name' => 'DashboardController',
            'active' => $route,
            'form' => $form,
            'passed' => $this->dashboardService->getPassed($route, $this->getUser()),
            'expected' => $this->dashboardService->getExpected($route, $this->getUser()),
        ]);
    }

    #[isGranted(User::ROLE_ADMIN)]
    private function renderAdmin(): Response
    {
        $activeRoute = $this->dashboardService->getActiveRoute();

        $view = match ($activeRoute){
            null => $this->render('dashboard/index.html.twig', [
                'controller_name' => 'DashboardController',
                'routes' => $this->getRouteWithForms(),
            ]),
            default => $this->render('dashboard/active.html.twig', [
                'controller_name' => 'DashboardController',
                'active' => $activeRoute,
                'recap' => $this->dashboardService->getRouteRecap($activeRoute),
            ])
        };

        if(isset($this->redirect)){
            return $this->redirect;
        }

        return $view;
    }

    private function getRouteWithForms(): array
    {
        return array_map(function ($r){
            $form = $this->createForm(RouteStartType::class, $r);
            $form->handleRequest($this->request);
            if ($form->isSubmitted() && $form->isValid()) {
                if($this->request->get('route_start')['start-id'] == $r->getId()) {
                    /** @var RouteEntity $route */
                    $route = $form->getData();
                    $route->setActive(true);
                    $this->routeRepository->save($route, true);
                    $this->redirect = $this->redirectToRoute('app_dashboard');
                }
            }
            return [
                'route' => $r,
                'form' => $form->createView(),
            ];
        }, $this->dashboardService->getRoutes());
    }

    #[isGranted(User::ROLE_ADMIN)]
    #[Route('/dashboard/deactivate/{id}', name: 'app_deactivate_route')]
    public function makeInactive(RouteEntity $route): Response
    {
        $route->setActive(false);
        $route->setNumberOfPlayers(null);
        $this->routeRepository->save($route, true);
        return $this->redirectToRoute('app_dashboard');
    }
}
