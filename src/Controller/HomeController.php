<?php

namespace App\Controller;

use App\Service\ReportingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $report = [];
        $fs = new Filesystem();
        if($fs->exists(ReportingService::REPORT_NAME)){
            $report = json_decode(file_get_contents(ReportingService::REPORT_NAME), true);
        }
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'report' => $report,
        ]);
    }
}
