<?php

namespace App\Form;

use App\Entity\Route;
use Carbon\Carbon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RouteReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('route', EntityType::class, [
                'class' => Route::class,
                'choice_label' => 'name',
            ])
            ->add('start', DateType::class, [
                'label' => 'Начална дата',
                'html5' => false,
                'data' => Carbon::now(new \DateTimeZone('Europe/Sofia'))->subDays(),
            ])
            ->add('end', DateType::class, [
                'label' => 'Крайна дата',
                'html5' => true,
                'data' => Carbon::now(new \DateTimeZone('Europe/Sofia')),
            ])
            ->add('publish', CheckboxType::class, [
                'label' => 'Публикувай',
                'required' => false,
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
