<?php

namespace App\Entity;

use App\Repository\RouteUserRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RouteUserRepository::class)]
#[ORM\UniqueConstraint(
    name: 'route_user_idx',
    columns: ['route_id', 'user_id']
)]
#[ORM\UniqueConstraint(
    name: 'route_number_idx',
    columns: ['route_id', 'number']
)]
class RouteUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'routeUsers')]
    private ?Route $route = null;

    #[ORM\ManyToOne(inversedBy: 'routeUsers')]
    private ?User $user = null;

    #[ORM\Column]
    private ?int $number = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoute(): ?Route
    {
        return $this->route;
    }

    public function setRoute(?Route $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }
}
