<?php

namespace App\Entity;

use App\Enum\UserRoleEnum;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const ROLE_USER      = 'ROLE_USER';
    public const ROLE_ADMIN     = 'ROLE_ADMIN';

    public const ROLES = [
        self::ROLE_USER => 'Потребител',
        self::ROLE_ADMIN => 'Администратор',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $username = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: RouteUser::class)]
    private Collection $routeUsers;

    public function __construct()
    {
        $this->routeUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    public function addRole(string $role): self
    {
        $this->roles = array_merge($this->roles, [$role]);

        return $this;
    }

    public function removeRole(string $role): self
    {
        $this->roles = array_filter($this->roles, function ($r) use ($role){
            return $r != $role;
        });

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, RouteUser>
     */
    public function getRouteUsers(): Collection
    {
        return $this->routeUsers;
    }

    public function addRouteUser(RouteUser $routeUser): self
    {
        if (!$this->routeUsers->contains($routeUser)) {
            $this->routeUsers->add($routeUser);
            $routeUser->setUser($this);
        }

        return $this;
    }

    public function removeRouteUser(RouteUser $routeUser): self
    {
        if ($this->routeUsers->removeElement($routeUser)) {
            // set the owning side to null (unless already changed)
            if ($routeUser->getUser() === $this) {
                $routeUser->setUser(null);
            }
        }

        return $this;
    }
}
