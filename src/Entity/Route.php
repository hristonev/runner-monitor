<?php

namespace App\Entity;

use App\Repository\RouteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RouteRepository::class)]
class Route
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'route', targetEntity: RouteUser::class)]
    #[ORM\OrderBy(['number' => 'ASC'])]
    private Collection $routeUsers;

    #[ORM\Column]
    private bool $active;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $numberOfPlayers;

    public function __construct()
    {
        $this->routeUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, RouteUser>
     */
    public function getRouteUsers(): Collection
    {
        return $this->routeUsers;
    }

    public function addRouteUser(RouteUser $routeUser): self
    {
        if (!$this->routeUsers->contains($routeUser)) {
            $this->routeUsers->add($routeUser);
            $routeUser->setRoute($this);
        }

        return $this;
    }

    public function removeRouteUser(RouteUser $routeUser): self
    {
        if ($this->routeUsers->removeElement($routeUser)) {
            // set the owning side to null (unless already changed)
            if ($routeUser->getRoute() === $this) {
                $routeUser->setRoute(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getNumberOfPlayers(): ?int
    {
        return $this->numberOfPlayers;
    }

    public function setNumberOfPlayers(?int $numberOfPlayers): void
    {
        $this->numberOfPlayers = $numberOfPlayers;
    }
}
