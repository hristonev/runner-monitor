<?php

namespace App\Repository;

use App\Entity\Checkpoint;
use App\Entity\Route;
use App\Entity\RouteUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Checkpoint>
 *
 * @method Checkpoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Checkpoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Checkpoint[]    findAll()
 * @method Checkpoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CheckpointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Checkpoint::class);
    }

    public function save(Checkpoint $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Checkpoint $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getByPeriod(Route $route, $start, $end)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin(
                RouteUser::class,
                'u',
                Join::WITH,
                'u.user = c.user AND u.route = c.route'
            )
            ->andWhere('c.route = :route')
            ->andWhere('c.created BETWEEN :start AND :end')
            ->setParameter('route', $route)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->addOrderBy('c.player', 'ASC')
            ->addOrderBy('u.number', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return Checkpoint[] Returns an array of Checkpoint objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Checkpoint
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
