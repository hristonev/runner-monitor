<?php

namespace App\Service;

class SecurityService
{
    public function randomPassword(string $charset = '0123456789abcdefghijklmnopqrstuvwxyz', $length = 6): string
    {
        $charsetMax = strlen($charset) - 1;

        $parts = array_map(function () use ($charset, $charsetMax){
            return $charset[mt_rand(0, $charsetMax)];
        }, array_fill(0, $length, 0));

        return implode('', $parts);
    }
}