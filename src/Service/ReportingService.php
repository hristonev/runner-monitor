<?php

namespace App\Service;

use App\Entity\Checkpoint;
use App\Entity\Route;
use App\Repository\RouteUserRepository;
use Symfony\Component\Filesystem\Filesystem;

class ReportingService
{
    public function __construct(
        private readonly RouteUserRepository $routeUserRepository,
    ){}

    public const REPORT_NAME = 'report/report.json';

    public function publishCheckpointData(Route $route, array $checkpoints): void
    {
        $data = [];

        /** @var Checkpoint $checkpoint */
        foreach ($checkpoints as $checkpoint){
            if(!array_key_exists($checkpoint->getPlayer(), $data)){
                $data[$checkpoint->getPlayer()] = [];
            }
            $passed = $checkpoint->getCreated();
            $passed->setTimezone(new \DateTimeZone('Europe/Sofia'));
            $data[$checkpoint->getPlayer()][] = [
                'checkpoint' => $checkpoint->getUser()->getUsername(),
                'passed' => $passed->format('H:i:s'),
                'player' => $checkpoint->getPlayer(),
            ];
        }

        $posts = array_map(function ($ru){
            return $ru->getUser()->getUsername();
        }, $this->routeUserRepository->findBy(
            ['route' => $route],
            ['number' => 'ASC']
        ));

        $fs = new Filesystem();
        $fs->dumpFile(self::REPORT_NAME, json_encode([
            'players' => array_values($data),
            'checkpoints' => $posts,
        ]));
    }
}