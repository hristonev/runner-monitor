<?php

namespace App\Service;

use App\Entity\Checkpoint;
use App\Entity\Route;
use App\Repository\CheckpointRepository;
use App\Repository\RouteRepository;
use App\Repository\RouteUserRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardService
{
    /** @var Checkpoint[] */
    private array $passed = [];

    public function __construct(
        private readonly RouteRepository $routeRepository,
        private readonly CheckpointRepository $checkpointRepository,
        private readonly RouteUserRepository $routeUserRepository,
    ){}

    public function getRoutes(): array
    {
        return $this->routeRepository->findAll();
    }

    public function getActiveRoute(): ?Route
    {
        return $this->routeRepository->findOneBy([
            'active' => true,
        ]);
    }

    public function getRouteRecap(Route $route): array
    {

        return [];
    }

    public function getPassed(?Route $route, UserInterface $user): array
    {
        $this->passed = $this->checkpointRepository->findBy([
            'user' => $user,
        ]);
        return $this->passed;
    }

    public function getExpected(?Route $route, UserInterface $user): array
    {
        if(count($this->passed) <= 0){
            $this->getPassed($route, $user);
        }

        $routeUser = $this->routeUserRepository->findOneBy([
            'user' => $user,
            'route' => $route,
        ]);
        if(!$routeUser){
            return [];
        }
        $expected = match($routeUser->getNumber()){
            1 => array_keys(array_fill(1, $route->getNumberOfPlayers(), 0)),
            default => $this->getExpectedByNumber($route, $user, $routeUser->getNumber() - 1),
        };

        $passed = array_map(function ($c){
            return $c->getPlayer();
        }, $this->passed);

        return array_filter($expected, function ($e) use ($passed){
            return !in_array($e, $passed);
        });
    }

    private function getExpectedByNumber(?Route $route, UserInterface $user, int $number): array
    {
        $routeUser = $this->routeUserRepository->findOneBy([
            'route' => $route,
            'number' => $number,
        ]);

        return array_map(function($c){
            return $c->getPlayer();
        }, $this->checkpointRepository->findBy([
            'route' => $routeUser->getRoute(),
            'user' => $routeUser->getUser(),
        ]));
    }
}